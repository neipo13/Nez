﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Nez.BitmapFonts;

namespace Nez.UI
{
    public class SelectBoxScroll : Element, IGamepadFocusable
    {
        string[] _items = new string[0];
        public string SelectedItem => _items.Length > 0 ? _items[index] : null;

        int _index = 0;

        float _prefWidth, _prefHeight;
        bool _isDisabled, _isFocused;
        SelectBoxStyle style;
        Action onSelectionChanged = null;

        public bool centerAlignText = false;


        public SelectBoxScroll(Skin skin) : this(skin.get<SelectBoxStyle>(), null)
        { }


        public SelectBoxScroll(Skin skin, string styleName = null) : this(skin.get<SelectBoxStyle>(styleName), null)
        { }


        public SelectBoxScroll(SelectBoxStyle style, Action action = null)
        {
            onSelectionChanged = action;
            setStyle(style);
            setSize(preferredWidth, preferredHeight);            
        }


        public override void layout()
        {
            var bg = style.background;
            var font = style.font;

            if (bg != null)
                _prefHeight = Math.Max(bg.topHeight + bg.bottomHeight + font.lineHeight - font.descent * 2f, bg.minHeight);
            else
                _prefHeight = font.lineHeight - font.descent * 2;

            float maxItemWidth = 0;
            for (var i = 0; i < _items.Length; i++)
                maxItemWidth = Math.Max(font.measureString(_items[i].ToString()).X, maxItemWidth);


            _prefWidth = maxItemWidth;
            if (bg != null)
                _prefWidth += bg.leftWidth + bg.rightWidth;

            var listStyle = style.listStyle;
            var scrollStyle = style.scrollStyle;
            float listWidth = maxItemWidth + listStyle.selection.leftWidth + listStyle.selection.rightWidth;
            if (scrollStyle.background != null)
                listWidth += scrollStyle.background.leftWidth + scrollStyle.background.rightWidth;
            _prefWidth = Math.Max(_prefWidth, listWidth);
        }


        public override void draw(Graphics graphics, float parentAlpha)
        {
            validate();

            IDrawable background;
            if (_isDisabled && style.backgroundDisabled != null)
                background = style.backgroundDisabled;
            else if (_isFocused && style.backgroundOver != null)
                background = style.backgroundOver;
            else if (style.background != null)
                background = style.background;
            else
                background = null;

            var font = style.font;
            var fontColor = style.fontColor;
            if(_isDisabled && style.disabledFontColor != null)
            {
                fontColor = style.disabledFontColor;
            }
            else if(_isFocused && style.overFontColor != null)
            {
                fontColor = style.overFontColor.Value;
            }
            

            var color = getColor();
            color = new Color(color, color.A * parentAlpha);
            float x = getX();
            float y = getY();
            float width = getWidth();
            float height = getHeight();

            if (centerAlignText)
            {
                x -= style.font.measureString(SelectedItem).X / 2;
                y -= height / 2;
            }

            if (background != null)
                background.draw(graphics, x, y, width, height, color);

            //Look at Progress bar for ideas on handling the "knobs"
            IDrawable leftArrow, rightArrow;
            if(style.scrollStyle.hScrollKnob != null)
            {
                leftArrow = style.scrollStyle.hScrollKnob;
                rightArrow = style.scrollStyle.hScrollKnob;
            }

            if (SelectedItem != null)
            {
                var str = SelectedItem.ToString();
                if (background != null)
                {
                    width -= background.leftWidth + background.rightWidth;
                    height -= background.bottomHeight + background.topHeight;
                    x += background.leftWidth;
                    y += (int)(height / 2 + background.bottomHeight - font.lineHeight / 2);
                }
                else
                {
                    y += (int)(height / 2 + font.lineHeight / 2);
                }

                fontColor = new Color(fontColor, fontColor.A * parentAlpha);
                graphics.batcher.drawString(font, str, new Vector2(x, y), fontColor);
            }
        }

        public void setStyle(SelectBoxStyle style)
        {
            Assert.isNotNull(style, "style cannot be null");
            this.style = style;
            invalidateHierarchy();
        }

        public override float preferredWidth
        {
            get
            {
                validate();
                return _prefWidth;
            }
        }

        public override float preferredHeight
        {
            get
            {
                validate();
                return _prefHeight;
            }
        }

        public int index
        {
            get
            {
                return _index;
            }
            set
            {
                if(value >= _items.Length)
                {
                    _index = 0;
                }
                else if(value < 0)
                {
                    _index = _items.Length - 1;
                }
                else
                {
                    _index = value;
                }
            }
        }

        /// <summary>
        /// sets the item list and resets the index to 0
        /// </summary>
        /// <param name="items"></param>
        public void SetItems(string[] items)
        {
            this._items = items;
            index = 0;
            float oldPrefWidth = preferredWidth;

            invalidate();
            validate();
            if (oldPrefWidth != _prefWidth)
            {
                invalidateHierarchy();
                setSize(_prefWidth, _prefHeight);
            }
        }

        public void SetSelectedItem(string item)
        {
            var foundIndex = Array.FindIndex(_items, i => i == item);
            this.index = foundIndex;
            onSelectionChanged?.Invoke();
        }

        #region IGamepadFocusable
        public bool shouldUseExplicitFocusableControl { get; set; }
        public IGamepadFocusable gamepadUpElement { get; set; }
        public IGamepadFocusable gamepadDownElement { get; set; }
        public IGamepadFocusable gamepadLeftElement { get; set; }
        public IGamepadFocusable gamepadRightElement { get; set; }

        public void enableExplicitFocusableControl(IGamepadFocusable upEle, IGamepadFocusable downEle, IGamepadFocusable leftEle, IGamepadFocusable rightEle)
        {
            shouldUseExplicitFocusableControl = true;
            gamepadUpElement = upEle;
            gamepadDownElement = downEle;
        }

        public void onActionButtonPressed() { }

        public void onActionButtonReleased() { }

        public void onFocused()
        {
            //show arrows
            _isFocused = true;
        }

        public void onUnfocused()
        {
            //hide arrows
            _isFocused = false;
        }

        /// <summary>
        /// This is where we in/decrement the index
        /// </summary>
        /// <param name="direction"></param>
        public void onUnhandledDirectionPressed(Direction direction)
        {
            if (direction == Direction.Right)
            {
                index += 1;
                onSelectionChanged?.Invoke();
            }
            else if(direction == Direction.Left)
            {
                index -= 1;
                onSelectionChanged?.Invoke();
            }
            validate();
        }
        #endregion
    }
}
