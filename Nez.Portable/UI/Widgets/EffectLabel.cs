﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Nez.BitmapFonts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Nez.UI.Widgets
{
    public enum TextEffect
    {
        None,
        Shake,
        Wave,
        Rainbow,
        Pulse,
        Rotate,
        Color
    }
    public class EffectLabel : Element
    {
        public SoundEffect talkingSound;
        public override float preferredWidth
        {
            get
            {
                if (_wrapText)
                    return 0;

                if (_prefSizeInvalid)
                    computePrefSize();

                var w = _prefSize.X;
                if (_style.background != null)
                    w += _style.background.leftWidth + _style.background.rightWidth;
                return w;
            }
        }

        public override float preferredHeight
        {
            get
            {
                if (_prefSizeInvalid)
                    computePrefSize();

                var h = _prefSize.Y;
                if (_style.background != null)
                    h += _style.background.topHeight + _style.background.bottomHeight;
                return h;
            }
        }


        // configuration
        LabelStyle _style;
        string _text;
        float _fontScaleX = 1;
        float _fontScaleY = 1;
        int labelAlign = AlignInternal.left;
        //int lineAlign = AlignInternal.left;
        string _ellipsis;
        bool _wrapText;
        float _effectForce = 2f;
        float _effectShiftSpeed = 5f;
        char delimiter = '/';
        Color highlightColor = Color.Red;

        // internal state
        string _wrappedString;
        bool _prefSizeInvalid;
        float _lastPrefHeight;
        Vector2 _prefSize;
        Vector2 _textPosition;


        


        public EffectLabel(string text, LabelStyle style)
        {
            setStyle(style);
            setText(text);
            touchable = Touchable.Disabled;
        }


        public EffectLabel(string text, Skin skin, string styleName = null) : this( text, skin.get<LabelStyle>( styleName ) )
		{ }


        public EffectLabel(string text, BitmapFont font, Color fontColor) : this( text, new LabelStyle( font, fontColor ) )
		{ }


        public EffectLabel(string text, BitmapFont font) : this( text, font, Color.White )
		{ }


        public EffectLabel(string text) : this( text, Graphics.instance.bitmapFont )
		{ }


        public virtual EffectLabel setStyle(LabelStyle style)
        {
            _style = style;
            invalidateHierarchy();
            return this;
        }


        /// <summary>
        /// Returns the button's style. Modifying the returned style may not have an effect until {@link #setStyle(ButtonStyle)} is called.
        /// </summary>
        /// <returns>The style.</returns>
        public virtual LabelStyle getStyle()
        {
            return _style;
        }


        public override void invalidate()
        {
            base.invalidate();
            _prefSizeInvalid = true;
        }


        void computePrefSize()
        {
            _prefSizeInvalid = false;
            var rgx = new Regex("/[0-9]");
            var textLessMarkup = rgx.Replace(_text, "");
            var wrappedForSizing = _wrappedString;
            if (_wrapText && _ellipsis == null && width > 0)
            {
                var widthCalc = width;
                if (_style.background != null)
                    widthCalc -= _style.background.leftWidth + _style.background.rightWidth;

                _wrappedString = _style.font.wrapText(_text, widthCalc / _fontScaleX);
                wrappedForSizing = _style.font.wrapText(textLessMarkup, widthCalc / _fontScaleX);
            }
            else
            {
                _wrappedString = _text;
                wrappedForSizing = textLessMarkup;
            }

            _prefSize = _style.font.measureString(wrappedForSizing) * new Vector2(_fontScaleX, _fontScaleY);
        }


        #region Configuration
        

        public EffectLabel setText(string text)
        {
            if (_text != text)
            {
                _wrappedString = null;
                _text = text;
                _prefSizeInvalid = true;
                invalidateHierarchy();
            }
            return this;
        }

        public EffectLabel setDelimiter(char delim)
        {
            delimiter = delim;
            return this;
        }

        public EffectLabel setHightlightColor(Color color)
        {
            if (highlightColor != color)
            {
                _wrappedString = null;
                highlightColor = color;
                _prefSizeInvalid = true;
                invalidateHierarchy();
            }
            return this;
        }

        public char getDelimiter()
        {
            return delimiter;
        }


        public string getText()
        {
            return _text;
        }

        public Color getHighlightColor() { return highlightColor; }


        /// <summary>
        /// background may be null to clear the background.
        /// </summary>
        /// <returns>this</returns>
        /// <param name="background">Background.</param>
        public EffectLabel setBackground(IDrawable background)
        {
            _style.background = background;
            invalidate();
            return this;
        }


        /// <summary>
        /// alignment Aligns all the text within the label (default left center) and each line of text horizontally (default left)
        /// </summary>
        /// <param name="alignment">Alignment.</param>
        public EffectLabel setAlignment(Align alignment)
        {
            return setAlignment(alignment, alignment);
        }


        /// <summary>
        /// labelAlign Aligns all the text within the label (default left center).
        /// lineAlign Aligns each line of text horizontally (default left).
        /// </summary>
        /// <param name="labelAlign">Label align.</param>
        /// <param name="lineAlign">Line align.</param>
        public EffectLabel setAlignment(Align labelAlign, Align lineAlign)
        {
            this.labelAlign = (int)labelAlign;

            // TODO
            //			var tempLineAlign = (int)lineAlign;
            //			if( ( tempLineAlign & AlignInternal.left ) != 0 )
            //				this.lineAlign = AlignInternal.left;
            //			else if( ( tempLineAlign & AlignInternal.right ) != 0 )
            //				this.lineAlign = AlignInternal.right;
            //			else
            //				this.lineAlign = AlignInternal.center;

            invalidate();
            return this;
        }


        public EffectLabel setFontColor(Color color)
        {
            _style.fontColor = color;
            return this;
        }


        public EffectLabel setFontScale(float fontScale)
        {
            _fontScaleX = fontScale;
            _fontScaleY = fontScale;
            invalidateHierarchy();
            return this;
        }


        public EffectLabel setFontScale(float fontScaleX, float fontScaleY)
        {
            _fontScaleX = fontScaleX;
            _fontScaleY = fontScaleY;
            invalidateHierarchy();
            return this;
        }


        /// <summary>
        /// When non-null the text will be truncated "..." if it does not fit within the width of the label. Wrapping will not occur
        /// when ellipsis is enabled. Default is false.
        /// </summary>
        /// <param name="ellipsis">Ellipsis.</param>
        public EffectLabel setEllipsis(String ellipsis)
        {
            _ellipsis = ellipsis;
            return this;
        }


        /// <summary>
        /// When true the text will be truncated "..." if it does not fit within the width of the label. Wrapping will not occur when
        /// ellipsis is true. Default is false.
        /// </summary>
        /// <param name="ellipsis">Ellipsis.</param>
        public EffectLabel setEllipsis(bool ellipsis)
        {
            if (ellipsis)
                _ellipsis = "...";
            else
                _ellipsis = null;
            return this;
        }


        /// <summary>
        /// should the text be wrapped?
        /// </summary>
        /// <param name="shouldWrap">If set to <c>true</c> should wrap.</param>
        public EffectLabel setWrap(bool shouldWrap)
        {
            _wrapText = shouldWrap;
            invalidateHierarchy();
            return this;
        }

        #endregion


        public override void layout()
        {
            if (_prefSizeInvalid)
                computePrefSize();

            var isWrapped = _wrapText && _ellipsis == null;
            if (isWrapped)
            {
                if (_lastPrefHeight != preferredHeight)
                {
                    _lastPrefHeight = preferredHeight;
                    invalidateHierarchy();
                }
            }

            var width = this.width;
            var height = this.height;
            _textPosition.X = 0;
            _textPosition.Y = 0;
            // TODO: explore why descent causes mis-alignment
            //_textPosition.Y =_style.font.descent;
            if (_style.background != null)
            {
                _textPosition.X = _style.background.leftWidth;
                _textPosition.Y = _style.background.topHeight;
                width -= _style.background.leftWidth + _style.background.rightWidth;
                height -= _style.background.topHeight + _style.background.bottomHeight;
            }

            float textWidth, textHeight;
            if (isWrapped || _wrappedString.IndexOf('\n') != -1)
            {
                // If the text can span multiple lines, determine the text's actual size so it can be aligned within the label.
                textWidth = _prefSize.X;
                textHeight = _prefSize.Y;

                if ((labelAlign & AlignInternal.left) == 0)
                {
                    if ((labelAlign & AlignInternal.right) != 0)
                        _textPosition.X += width - textWidth;
                    else
                        _textPosition.X += (width - textWidth) / 2;
                }
            }
            else
            {
                textWidth = width;
                textHeight = _style.font.lineHeight * _fontScaleY;
            }

            if ((labelAlign & AlignInternal.bottom) != 0)
            {
                _textPosition.Y += height - textHeight;
                y += _style.font.descent;
            }
            else if ((labelAlign & AlignInternal.top) != 0)
            {
                _textPosition.Y += 0;
                y -= _style.font.descent;
            }
            else
            {
                _textPosition.Y += (height - textHeight) / 2;
            }

            //_textPosition.Y += textHeight;

            // if we have GlyphLayout this code is redundant
            if ((labelAlign & AlignInternal.left) != 0)
                _textPosition.X = 0;
            else if (labelAlign == AlignInternal.center)
                _textPosition.X = width / 2 - (_prefSize.X / 2); // center of width - center of text size
            else
                _textPosition.X = width - _prefSize.X; // full width - our text size
        }


        public override void draw(Graphics graphics, float parentAlpha)
        {
            validate();

            var color = new Color(this.color, this.color.A * parentAlpha);
            if (_style.background != null)
                _style.background.draw(graphics, x, y, width == 0 ? _prefSize.X : width, height, color);
            var characters = _wrappedString.ToCharArray();
            var lastCharacterWasDelimiter = false;
            var effectString = "";
            var effectList = new List<TextEffect>();
            var verticalOffset = 0;
            var horizontalOffset = 0f;
            BitmapFontRegion currentFontRegion = null;
            for (var i = 0; i < characters.Length; i++)
            {
                var c = characters[i];
                if (c == '\r')
                    continue;
                if (c == '\n')
                {
                    verticalOffset++;
                    horizontalOffset = 0f;
                    currentFontRegion = null;
                    continue;
                }
                if (c == delimiter)
                {
                    if (!lastCharacterWasDelimiter)
                    {
                        lastCharacterWasDelimiter = true;
                        continue;
                    }
                }
                if (lastCharacterWasDelimiter)
                {
                    int value = 0;
                    bool parsesAsInt = int.TryParse(c.ToString(), out value);
                    if (parsesAsInt)
                    {
                        effectString += c;
                        continue;
                    }
                    else
                    {
                        lastCharacterWasDelimiter = false;
                        if(!string.IsNullOrWhiteSpace(effectString))
                        {
                            value = int.Parse(effectString);
                            effectString = "";
                            var effect = (TextEffect)value;
                            var alreadyHasIt = effectList.Contains(effect);
                            if (alreadyHasIt)
                            {
                                effectList.Remove(effect);
                            }
                            else
                            {
                                effectList.Add(effect);
                            }

                        }

                    }
                }
                if (c == delimiter)
                {
                    lastCharacterWasDelimiter = true;
                    continue;
                }
                if(c == ' ' && currentFontRegion != null)
                {
                    horizontalOffset += currentFontRegion.xAdvance * _fontScaleX;
                    currentFontRegion = null;
                }
                if (currentFontRegion != null)
                    horizontalOffset += currentFontRegion.xAdvance * _fontScaleX;
                _style.font.tryGetFontRegionForChar(c, out currentFontRegion, true);

                horizontalOffset += _style.font.spacing * _fontScaleX;
                var shift = (float)Math.Sin(_effectShiftSpeed * Time.time + horizontalOffset);
                var pos = new Vector2(x, y) + _textPosition + new Vector2(horizontalOffset, verticalOffset * _style.font.lineHeight * _fontScaleY);
                var fontColor = _style.fontColor;
                float rotation = 0f;
                Vector2 origin = Vector2.Zero;
                Vector2 scale = new Vector2(_fontScaleX, _fontScaleY);

                for(int j = 0; j < effectList.Count; j++)
                {
                    switch (effectList[j])
                    {
                        case TextEffect.Pulse:
                            scale += (new Vector2(Math.Abs(shift), Math.Abs(shift)) * _effectForce * 0.2f);
                            break;
                        case TextEffect.Wave:
                            pos += new Vector2(0, shift) * _effectForce;
                            break;
                        case TextEffect.Shake:
                            pos += new Vector2(Nez.Random.range(-1, 1), Nez.Random.range(-1, 1));
                            break;
                        case TextEffect.Rotate:
                            var rotationOffset = new Vector2(_style.font.spaceWidth / 2, _style.font.lineHeight / 2);
                            origin = rotationOffset;
                            pos += rotationOffset;
                            rotation = shift;
                            break;
                        case TextEffect.Color:
                            fontColor = highlightColor;
                            break;
                        case TextEffect.Rainbow:
                            //double->color from https://www.particleincell.com/2014/colormap/
                            var a = (1 - Math.Abs(shift)) / 0.25; //invert and group
                            var X = (int)Math.Floor(a);  //this is the integer part
                            var Y = (int)Math.Floor(255 * (a - X)); //fractional part from 0 to 255
                            var r = 0;
                            var g = 0;
                            var b = 0;
                            switch (X)
                            {
                                case 0: r = 255; g = Y; b = 0; break;
                                case 1: r = 255 - Y; g = 255; b = 0; break;
                                case 2: r = 0; g = 255; b = Y; break;
                                case 3: r = 0; g = 255 - Y; b = 255; break;
                                case 4: r = 0; g = 0; b = 255; break;
                            }
                            var colorz = new Color(r, g, b);
                            fontColor = colorz;
                            break;
                        case TextEffect.None:
                        default:
                            break;
                    }
                }

                graphics.batcher.drawString(
                    _style.font,
                    characters[i].ToString(),
                    pos,
                    fontColor,
                    rotation,
                    origin,
                    scale,
                    SpriteEffects.None,
                    0);

            }

        }
        

    }

    public class EffectLabelCharacter
    {
        public char c { get; set; }
        public TextEffect[] Effect { get; set; }

    }
}
